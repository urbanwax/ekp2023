# Python Decorator

Enhance Python Functions. Decorator wraps functions or methods in python, but does not modify its source code. Thus it modify or enhance the behaviour of funcs or methods.

## Definition
A decorator is a function(or a class) that takes as an argument another function and returns a new function that usually modifies teh behaviour of the original function.

## Decorating a function
Decorating multiple functions

```python
def decorator(func):
    def wrapper():
        print("I am changing the behaviour of the decorated function")
        func()
    return wrapper

@decorator
def decorated_function_1():
    print("Decorated Function 1")


@decorator
def decorated_function_2():
    print("Decorated Function 2")


decorated_function_1()
decorated_function_2()
```

Decorating functions with args and kwargs. We do not care how many and what kind of arguments we have
```python
def decorator_args(func):
    def wrapper(*args, **kwargs):
        # Add code here for decorator runtime before function call
        result = func(*args, **kwargs)
        # Add code here for decorator runtime after function call
        return result
    return wrapper

@decorator_args
def decorated_function(arg1, arg2, arg3):
    # You function code goes here
    pass

decorated_function("val1", 2, arg3="val3")
```
