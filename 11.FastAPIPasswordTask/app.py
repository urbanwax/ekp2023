from fastapi import FastAPI, HTTPException
from password import PasswordGenerator

app = FastAPI()


@app.get("/simple_password")
def simple():
    return {"simple_password": PasswordGenerator.simple_password()}


@app.get("/custom_password")
def gen_one_custom_password(upper_chars: bool,
                            lower_chars: bool,
                            digit_chars: bool,
                            length: int):
    password = PasswordGenerator.custom_password(lower_chars, upper_chars, digit_chars, length)
    if password:
        return {"password": password}
    else:
        return HTTPException(status_code=400, detail="Invalid parameters for password generation!")


@app.get("/custom_passwords")
def gen_multiple_custom_passwords(upper_chars: bool,
                                  lower_chars: bool,
                                  digit_chars: bool,
                                  length: int,
                                  number: int):
    passwords = PasswordGenerator.custom_password(lower_chars, upper_chars, digit_chars, length, number)
    if passwords:
        return {"passwords": passwords}
    else:
        return HTTPException(status_code=400, detail="Invalid parameters for passwords generation!")


@app.get("/passwords/number/{pass_num}/len/{pass_len}/upper/{up}/lower/{low}/digits/{dig}")
def gen_passwords(pass_num: int,
                  pass_len: int,
                  up: bool,
                  low: bool,
                  dig: bool):
    passwords = PasswordGenerator.custom_password(low, up, dig, pass_len, pass_num)
    if passwords:
        return {"passwords": passwords}
    else:
        return HTTPException(status_code=400, detail="Invalid parameters for passwords generation!")
