from fastapi import FastAPI, HTTPException
from sqlalchemy import create_engine, Column, Integer, String
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker
from sqlalchemy.exc import SQLAlchemyError
from pydantic import BaseModel
import logging

# Create FastAPI object
api = FastAPI()

logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)
console_formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s')
file_formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(module)s - %(message)s')
console_logger = logging.StreamHandler()
file_logger = logging.FileHandler(filename="todo.log")
console_logger.setFormatter(console_formatter)
file_logger.setFormatter(file_formatter)
logger.addHandler(file_logger)
logger.addHandler(console_logger)


# SQL setup
SQLALCHEMY_DATABASE_URL = "sqlite:///./todo.db"
engine = create_engine(SQLALCHEMY_DATABASE_URL, connect_args={"check_same_thread": False})
Base = declarative_base()


# SQL Table
class Todo(Base):
    __tablename__ = "todo"

    id = Column(Integer, primary_key=True, index=True)
    title = Column(String, index=True)
    description = Column(String)
    priority = Column(String)


SessionLocal = sessionmaker(autocommit=False, autoflush=False, bind=engine)

# Create SQL Table
Base.metadata.create_all(bind=engine)


# Pydantic models
class TodoCreate(BaseModel):
    title: str
    description: str
    priority: str




# Rest API routers
@api.get("/todos")
def read_todos():
    logger.info("Getting all todos from database.")

    try:
        db = SessionLocal()
        todos = db.query(Todo).all()
        logger.info("Todos successfully retrieved")
        logger.debug(f"Obtained {len(todos)} number of todos from Database")
        return todos
    except SQLAlchemyError as sql_ex:
        error_msg = "An error occurred while retrieving todos from Database."
        logger.warning(error_msg)
        logger.error(sql_ex)
        raise HTTPException(400, detail=error_msg)
    except Exception as e:
        mesg = "Unhandled Error occured"
        logger.error(mesg)
        raise HTTPException(400, detail=mesg)


@api.post("/todos")
def create_todo(todo: TodoCreate):
    db = SessionLocal()
    new_todo = Todo(title=todo.title, description=todo.description, priority=todo.priority)
    db.add(new_todo)
    db.commit()
    db.refresh(new_todo)
    return new_todo

# create router to obtain specific todo by id
# include response model everywhere
# create router to filter by priority
# validate priority to accept only low,medium,high

