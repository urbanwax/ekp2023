from jinja2 import Environment, FileSystemLoader

if __name__ == '__main__':
    environment = Environment(loader=FileSystemLoader("templates/"))
    template = environment.get_template("web.html")
    webpage = {
        "title": "First WebPage",
        "greeting": "Hello everyone. This is our first html page generated with Jinja2 templating engine."
    }
    services = ["Python", "Java Script", "Ruby", "Java", "C#", "GoLang", "C++", "Node.Js"]
    developers = ['Ivo', 'Ivan', 'Maria', 'Nikol']
    html_source = template.render(website=webpage, services=services , qa = ['cucumber', 'manual qa'], developers=developers)
    with open("index.html", mode="w", encoding="utf-8") as webpage:
        webpage.write(html_source)
