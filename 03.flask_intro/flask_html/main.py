"""
Create a website for IT services organization
It needs to have a menu where menu items and urls will reside in python dictionary
Menu should consist of Home, About, Contact(phone number, email) and All the services (Text and level for 1 to 5) that we offer.
Content should be delivered from the same dictionary(think site text)

Make use of https://getbootstrap.com/

Make sure the site is dynamic so when we add new services in the dict they will appear in the website.
"""



from flask import Flask, render_template

app = Flask(__name__)

content_services = "Problem solving etc."

@app.route("/")
def index():
    return render_template("index.html", title="This is my title", services=content_services)


if __name__ == '__main__':
    app.run(debug=True)
