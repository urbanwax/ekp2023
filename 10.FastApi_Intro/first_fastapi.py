from fastapi import FastAPI, HTTPException
from enum import Enum

app = FastAPI()


class BookType(str, Enum):
    textbook = "textbook"
    humor = "humor"
    novel = "novel"
    kids = "kids"
    comic = "comic"


books_inv = [
    {"title": "Trimata Glupatsi", "year": 1996, "type": BookType.humor},
    {"title": "20 years later", "year": 2000, "type": BookType.novel},
    {"title": "The three musketeers", "year": 1996, "type": BookType.novel},
    {"title": "Peppa Pig", "year": 2021, "type": BookType.kids},
    {"title": "Baby Shark", "year": 2013, "type": BookType.kids},
    {"title": "Math Textbook", "year": 2024, "type": BookType.textbook}
    ]


@app.get("/")
def info():
    """Get Basic information for the Books' Fast API Service."""
    return {"Message": "Wellcome to Books Rest API!"}


@app.get("/books")
def list_books():
    """Return a list of all books in the inventory. Output is not paginated. Output can be very large."""
    return {"Books": books_inv}


@app.get("/books/{book_id}")
def list_book(book_id: int):
    """Return a book with the specified id. If such book does not exist HTTPExceptions is raised with status code 400."""
    if book_id not in range(0, len(books_inv)-1):
        raise HTTPException(status_code=400, detail=f"Book with id={book_id} not found!")
    return {"Book": books_inv[book_id]}


@app.get("/books/type/{book_type}")
def list_books_by_type(book_type: BookType):
    """Return a list of books with the specified type. Type is validated against BookType"""
    return [book for book in books_inv if book.get("type") == book_type]


@app.get("/books/search/")
def query_books(year: int | None = None, title: str | None = None):
    """
    Filter books list by year and/or title.
    Part of the title is also valid. Not full title required.
    Both year and title query parameters are optional. If not set a full book list is returned.
    """
    if year and title:
        return [book for book in books_inv if book.get("year") == year and title in book.get('title')]
    elif year:
        return [book for book in books_inv if book.get("year") == year]
    elif title:
        return [book for book in books_inv if title in book.get('title')]
    else:
        return books_inv
