"""
1. Define function that render simple html template string to html and return it as string.
2. Define function that render a html file template and returns it as a string.
3. Define function that render a html file template and saves it to a html file destination.
4. Decorate the two functions above to add html footer disclaimer.
    All Rights Reserved 2023. Example Company. Web-design by IT Step.
"""
from jinja2 import Environment, FileSystemLoader
import inspect


def add_disclaimer(template_func):
    def wrapper(*args, **kwargs):
        footer_str = "\n    <footer>All Rights Reserved 2023. Example Company. Web-design by IT Step.</footer>\n"
        out_file_arg = "output_file"

        def helper_footer(source: str) -> str:
            return source.replace("</body>", footer_str + "</body>")

        result = template_func(*args, **kwargs)
        # With next if we tackle Function 1 and 2
        if isinstance(result, str):
            result = helper_footer(result)
        # Get argnames
        argnames = inspect.getfullargspec(template_func).args
        print(f"Arguments of function{template_func} is {argnames}") # Just for description
        # With next if we tackle Function 3
        if out_file_arg in argnames:
            print(f"{out_file_arg} is part of function agrs {argnames}") # Just for description
            out_file_arg_value = args[argnames.index(out_file_arg)]
            print(f"Value of {out_file_arg} is {out_file_arg_value}") # Just for description
            if out_file_arg_value.split(".")[-1] != "html":
                return
            with open(out_file_arg_value, "r") as f:
                file_content = f.read()
            if file_content:
                file_content = helper_footer(file_content)
                with open(out_file_arg_value, "w") as f:
                    f.write(file_content)
        return result
    return wrapper


@add_disclaimer
def render_str_to_str(source: str, context: dict) -> str:
    env = Environment(loader=FileSystemLoader("."))
    template = env.from_string(source)
    return template.render(context)


@add_disclaimer
def render_file_to_str(source: str, context: dict) -> str:
    env = Environment(loader=FileSystemLoader("."))
    template = env.get_template(source)
    return template.render(context)


@add_disclaimer
def render_file_to_file(source: str, context: dict, output_file: str) -> None:
    env = Environment(loader=FileSystemLoader("."))
    template = env.get_template(source)
    with open(output_file, "w") as fp:
        fp.write(template.render(context))


if __name__ == '__main__':

    input_html = '''<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>{{ title }}</title>
</head>
<body>
    <h2>Welcome to {{ title }}</h2>
    <p>This is my dummy homepage. Blah Blah Blah.</p>
</body>
</html>'''
    ctx = {"title": "My Homepage"}
    # Call Function 1
    out = render_str_to_str(input_html, ctx)
    print(out)
    print(50*'+')
    # Call Function 2
    src = "input.html"
    out = render_file_to_str(src, ctx)
    print(out)
    print(50 * '+')
    # Call Function 3
    out = "output.html"
    render_file_to_file(src, ctx, out)
