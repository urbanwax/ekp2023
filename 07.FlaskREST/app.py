from flask import Flask, jsonify, request

api = Flask(__name__)

capabilities = ['Python', 'Java']


@api.route('/', methods=['GET'])
def get_api_info():
    """
    Generates basic information related to the REST API Service
    :return:
    json
    """
    api_info = {
        'version': '1.0',
        'Main Programing Language': 'Python',
        'Framework': 'Flask',
        'Author': 'IT Step Computer Academy',
        'Autor email': 'info@itstep.org',
    }
    return jsonify(api_info)

# Variant 1
# @api.route('/capabilities', methods=['GET'])
# def list_capabilities():
#     return jsonify(capabilities)
#
#
# @api.route('/capabilities', methods=['POST'])
# def add_capability():
#     new_capability = request.json
#     capabilities.append(new_capability)
#     return jsonify({'Added Capability': new_capability})

# Variant 2
# @api.route('/capabilities', methods=['GET', 'POST'])
# def capability_handler():
#     if request.method == 'POST':
#         new_capability = request.json
#         capabilities.append(new_capability)
#         return jsonify({'Added Capability': new_capability})
#     return jsonify(capabilities)


# Variant 3
@api.get('/capabilities')
def get_capabilities():
    return jsonify(capabilities)


@api.post('/capabilities')
def post_capabilities():
    new_capability = request.json
    capabilities.append(new_capability)
    return jsonify({'Added Capability': new_capability})


'''
1----
Output of Capabilities should 
[
"0": "Python",
"1": "Java"
]
without touching the capabilities list

2----
GET
/capability/1

output -> Java

3-----
DELETE /capability/1
then delete Java capability

4----
DELETE /capability/Java
then delete Java


prepare neccessary checks
If delteion return what is deleted

5----
do not allow duplicates
POST /capabilites -> to add multiple 
POST /capability -> to add single

'''

if __name__ == '__main__':
    api.run(debug=True)
