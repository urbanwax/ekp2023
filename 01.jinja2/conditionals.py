from jinja2 import Environment, FileSystemLoader

if __name__ == '__main__':
    # Initialize variable to be used within the Jinja2 Template
    person_name = "Ivo"
    person_height = 178

    # Configure Jinja2 Engine
    templates_path = "templates/"
    environment = Environment(loader=FileSystemLoader(templates_path))
    template = environment.get_template("height.txt")

    with open("height.txt", "w") as f:
        output = template.render(name=person_name, height=person_height)
        f.write(output)

