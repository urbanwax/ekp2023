import jinja2

if __name__ == '__main__':
    env = jinja2.Environment()
    template = env.from_string("Hello my name is {{ name }} and I am {{ age }} years old!")
    output = template.render(name="Ivo", age=39)
    print(output)
