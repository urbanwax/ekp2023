from jinja2 import Environment, FileSystemLoader

env = Environment(loader=FileSystemLoader("templates/"))
template = env.get_template("mobile_bill.txt")


def gen_bill_text(pr, fname, lname, amount):
    res = template.render(pr=pr, fname=fname, lname=lname, amount=amount)
    return res


def gen_all_texts(data_set):
    all_texts = []
    for record in data_set:
        content = gen_bill_text(record.get('pr'), record.get('fname'), record.get('lname'), record.get('amount'))
        all_texts.append(content)
    return all_texts


def export_to_files(bill_letters):
    for letter in bill_letters:
        filename = letter.split(',')[0][6:].replace(' ', '_')
        filename += '.txt'
        with open(filename, mode='w', encoding='utf-8') as bill_file:
            bill_file.write(letter)


if __name__ == '__main__':
    data_set = [
        {'pr': 'Mr', 'fname': 'Ivo', 'lname': 'Ivanov', 'amount': 32.45},
        {'pr': 'Mrs', 'fname': 'Maria', 'lname': 'Nikolova', 'amount': 22.99},
        {'pr': 'Mrs', 'fname': 'Veneta', 'lname': 'Hadzhidimitrova', 'amount': 123.12}
    ]

    letters = gen_all_texts(data_set)

    export_to_files(letters)