# Body - Multi Parameters

Types of Parameters:

    - Path
    - Query
    - Body

## Body Parameters
Body parameters are part of the request body. When FastAPI sees a pydantic object treats it automatically as body param.

```python
@app.post("/books/{book_id}")
def create_book(
        item_id: int,
        book: Book):
    books[item_id] = book
    return {"status": "success"}
```
where Book is a defined in the scope class that inherits Pydantic BaseModel
```python
class Book(BaseModel):
    title: str
    author: str
    year: int
```
Data validation is made against the pydantic model.

***Forcing body parameters***
If only one parameter is supplied to a FastAPi decorated function, FastAPI will treat the parameter as query parameter even if it is Pydantic Model.
To change this behavior use Annotated from typing and Body from FastAPI.

```python
from typing import Annotated
from fastapi import FastAPI, Body
from pydantic import BaseModel

api = FastAPI()

class Item(BaseModel):
    id: int
    name: str


@api.post("/item")
def create_car(item: Annotated[Item, Body()]):
    return {"added item": item}
```

## Example Data
Example data is an explicit example to be displayed e.g. in the Swagger. We declare example for a specific Pydantic Model that will be added to teh JSON Schema.
This can be achieved via model_config.

Sample model config for the above defined Pydantic Model ***Book***.
```python
    model_config = {
        "json_schema_extra": {
            "examples": [
                {
                    "title": "The Three Musketeers",
                    "author": "Alexandre Dumas",
                    "year": 1844,
                },
            ]
        }
    }
```

## Field

Field is imported from Pydantic not from FastApi.

Official documentation: https://docs.pydantic.dev/latest/concepts/fields/

```python
from pydantic import BaseModel, Field
```


### Examples
One use is to defined examples directly when declaring member variables for the pydantic model:

```python
class Book(BaseModel):
    title: str = Field(examples=["The Three Musketeers"])
    author: str = Field(examples=["Alexandre Dumas"])
    year: int = Field(examples=[1844])
```

### Numerical constraints

    gt = greater than
    lt = less than
    ge = greater or equal
    le = less or equal

```python
class ConstraintedModel(BaseModel):
    some_str: str
    minimum: int = Field(gt=100)
    maximum: float = Field(lt=0)
```

### String constraints

    min_length = minimum string length
    max_length = maximum string length

```python
class ConstraintedModel(BaseModel):
    some_str_min_3: str = Field(min_length=3)
    some_str_max_30: str = Field(min_length=30)
```

