from pydantic import BaseModel, ValidationError
from typing import List


class Student(BaseModel):
    id: int
    name: str
    grades: List[float]


sample_data = {
        'id': 3,
        'name': 'Ivo',
        'grades': ['good', '5', 'excelent'] # 'grades': [5.6, 7]
    }

if __name__ == '__main__':
    try:
        student_pydantic = Student(**sample_data)
        print(student_pydantic.dict())
    except ValidationError as v:
        print(v)
