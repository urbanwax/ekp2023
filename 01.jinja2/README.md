# Jinja2

## Introduction

https://jinja.palletsprojects.com/en/3.1.x/

Templating Engine

## Installation
Install with:
```sh
pip install Jinja2
```

Verify:
```sh
pip list
```

## Syntax

### Basics
Placeholder - replace with value
```jinja2
Hello {{ name }}1
```

```python
jinja2_str = "Hello {{ name }}!"
```

Jinja2.Environment.from_string - this is used when our input is a simple string 

FileSystemLoader - this is used when our input templates are files within a specific directory

Conditionals:

IF-ENDIF
```jinja2
Hello {{ name }},

{% if amount > 0 %}
You have won {{ amount }} EUR from our quiz.
{% endif %}
Thank you for participation.

Our pleasure,
Quiz teams
```

IF-ELSE-ENDIF
```jinja2
Hello {{ name }},

{% if amount > 0 %}
You have won {{ amount }} EUR from our quiz.
{% else %}
We are sorry please try again playing our quiz.
{% endif %}
Thank you for participation.
 
Our pleasure,
Quiz teams
```

IF-ELIF-ELSE-ENDIF
```jinja2
Hello {{ name }},

{% if amount > 100 %}
You have won the big price which is: {{ amount }} EUR from our quiz.
{% elfi amount > 50%}
You have won our smaller price. But Hey it is still a win.
{% else %}
We are sorry please try again playing our quiz.
{% endif %}
Thank you for participation.
 
Our pleasure,
Quiz teams
```

Check if variable is defined in Jinja2
```jinja2
Dear 
{% if name is defined %}
{{ name }}
{% else %}
Sir/Madam
{% endif %}
,

You have won the big price which is: {{ amount }} EUR from our quiz.

Thank you for participation.
 
Our pleasure,
Quiz teams

```


Boolean operators
```jinja2
{% if gender == 'Male' and age >= 18 %}
Hello sir {{ name }},
{% elif gender == 'Female' and age >= 18 %}
Hello madam {{ name }},
{% else %}
Hello {{ name }},
{% endif %}

Some text goes here.
 
Our pleasure,
Quiz teams
```

Jinja2 Loops
```jinja2
Our team consists of:
{% for name in names %}
{{ name }}
{% endfor %}
```

Dictionaries
```jinja2
Personal information:
name: {{ person.name }}
age: {{ person.age }}
```

Comments

Except the use for straight forward comments, this can be used when testing to disable part or parts of the code.
```jinja2
{# This is just a comment and will not be rendered to the output #}
Text goes here.
```

Filters
Filters over variables in Jinja2 are used like:
```jinja2
{{ variable_name|filter_name }}
```

Capitalize the first word of the variable string
```jinja2
{{ variable|capitalize }}
```

Convert to lowercase
```jinja2
{{ variable|lower }}
```

Convert to upper
```jinja2
{{ variable|upper }}
```

Get the list length
```jinja2
{{ list_var|length }}
```

Get the first list item
```jinja2
{{ list_var|first }}
```

Get the last list item
```jinja2
{{ list_var|last }}
```