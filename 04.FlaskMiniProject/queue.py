class InvQueue:
    def __init__(self):
        self._queue = []

    def is_empty(self):
        return len(self._queue) == 0

    def add(self, item):
        if item:
            self._queue.append(item)
            return item
        else:
            return None

    def get(self):
        if not self.is_empty():
            return self._queue.pop(0)
        else:
            return None

    def display_last(self):
        if not self.is_empty():
            return self._queue[0]
        return None

    def list(self):
        return self._queue


if __name__ == '__main__':
    my_inv = InvQueue()
    print(my_inv.is_empty())
    my_inv.add('Item 1')
    my_inv.add('Item 2')
    print(my_inv.list())
    print(my_inv.is_empty())
    print(my_inv.get())
    print(my_inv.get())
    print(my_inv.is_empty())
    print(my_inv.list())
