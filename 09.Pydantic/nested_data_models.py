from pydantic import BaseModel, ValidationError
from typing import List


class SchoolClass(BaseModel):
    grade: int
    division: str


class Student(BaseModel):
    id: int
    name: str
    grades: List[float]
    student_class: SchoolClass


sample_data = {
        'id': 3,
        'name': 'Ivo',
        'grades': ['5.6', 7],
        'student_class': {'grade': 8, 'division': 'b'}
    }

if __name__ == '__main__':
    try:
        student_pydantic = Student(**sample_data)
        print(student_pydantic.dict())
    except ValidationError as v:
        print(v)