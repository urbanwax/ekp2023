templates_dir = "templates/"
source_index = "source_index.html"
source_custom = "source_custom.html"
output_index = "output/index.html"
output_custom = "output/custom.html"
webpage = {
    "title": "First WebPage",
    "start_heading": "Welcome to out Hosting Provider.",
    "start_text": "We offer you a variety of hosting offerings, so you can choose teh one that best suits your needs."
}
services = [
    {
        "name": "Basic",
        "suits": "Perfect for small websites",
        "storage": 10,
        "ram": 1,
        "domains": 1,
        "bandwidth": "1000GB",
        "price": 9.99,
    },
    {
        "name": "Intermediate",
        "suits": "Perfect for medium websites and webapps",
        "storage": 30,
        "ram": 3,
        "domains": 5,
        "bandwidth": "Unlimited",
        "price": 19.99,
    },
    {
        "name": "Premium",
        "suits": "Perfect enterprise web apps",
        "storage": 100,
        "ram": 10,
        "domains": "Unlimited",
        "bandwidth": "Unlimited",
        "price": 59.99,
    },
]
custom = {
        "name": "Custom",
        "suits": "Perfect all your needs",
        "storage": [10, 20, 30],
        "ram": [1, 2, 5, 10],
        "domains": [1, 2, 5, "Unlimited"],
        "bandwidth": "Unlimited",
        "price": "Calculated later on",
    }
