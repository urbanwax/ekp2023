from sqlalchemy.orm import DeclarativeBase, Mapped, mapped_column, Session
import sqlalchemy

class Base(DeclarativeBase):
    pass


class Person(Base):
    __tablename__ = "orm_people"
    id: Mapped[int] = mapped_column(primary_key=True)
    first_name: Mapped[str] = mapped_column(sqlalchemy.String(20))
    last_name: Mapped[str] = mapped_column(sqlalchemy.String(30))

    def __repr__(self) -> str:
        return f"Person(id={self.id!r}, first_name={self.first_name!r}, last_name={self.last_name!r})"


engine = sqlalchemy.create_engine("sqlite:///my_database.sqlite")

conn = engine.connect()
meta = Base.metadata.create_all(engine)

with Session(engine) as s:
    person1 = Person(first_name="Ivan", last_name="Dimitrov")
    s.add(person1)
    s.commit()

s = Session(engine)
stmt = sqlalchemy.select(Person)

for user in s.execute(stmt):
    print(user)
