from flask import Flask
from string import ascii_lowercase, digits
from random import choice

app = Flask(__name__)


@app.route("/str/<int:strlen>")
def get_randstr(strlen):
    return ''.join([choice(ascii_lowercase+digits) for _ in range(strlen)])


if __name__ == '__main__':
    app.run(debug=True)