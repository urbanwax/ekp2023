# Flask mini project

This will be a web page in Flask.

The web page will be a simple car dealership.


## Menu
- Home
- Add Car
- Sell Car
- Sold Cars

Home will display in HTML a table with all the Available cars that can are up for sale.

Add Car will get all the parameters for a new car to be added to the inventory(Available Cars).

Sell Car will display the next car for sale (only the oldest car in the Available Cars list) and a button to perm the sell. The option shall be available only if the Available Car List is not Empty.

Sold Cars will display all the cars that are sold up to now.

## Car in Available Cars list
This will be a car that will be up for sale. Have in mind that the inventory will act as a FIFO object - thus only the first added car to the Available Cars list can be sold.

- ID (something unique to the car)
- Make
- Model
- Year
- Color
- picture (car picture can be added as an url somewhere from Internet)

## Car in Sold list
Will have all teh fields from Available Car list plus:
- Owner name
- Owner age

## Other Requirements
- For HTML use Bootstrap(or similar)
- Data set should reside in the program's memory(for now)
- All logic related to car inventory: both car lists and actions on cars need to be developed with Classes and Methods.
- 