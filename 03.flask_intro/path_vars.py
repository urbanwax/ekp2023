from flask import Flask

app = Flask(__name__)

@app.route('/')
def index():
    return "Welcome to our web site"


@app.route('/question')
def question():
    return "Please enter a keyword for you question like 127.0.0.1:5000/question/python, where 'python' is the keyword"


@app.route('/question/<keyword>')
def keyword_question(keyword):
    return f"Your question about {keyword}..."


if __name__ == '__main__':
    app.run(debug=True)