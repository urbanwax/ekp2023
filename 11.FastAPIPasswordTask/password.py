from random import choice, randint
from string import ascii_lowercase, ascii_uppercase, digits


class PasswordGenerator:
    @staticmethod
    def simple_password():
        return ''.join([choice(ascii_lowercase + digits) for _ in range(8)])

    @staticmethod
    def custom_password(lower_chars: bool = True,
                        upper_chars: bool = True,
                        digit_chars: bool = True,
                        length: int = 10,
                        number: int = 1):
        chars = ''
        if lower_chars:
            chars += ascii_lowercase
        if upper_chars:
            chars += ascii_uppercase
        if digit_chars:
            chars += digits
        if not chars or number < 1 or length < 5 or length > 50:
            return None
        if number > 1:
            return [''.join([choice(chars) for _ in range(length)]) for _ in range(number)]
        else:
            return ''.join([choice(chars) for _ in range(length)])
