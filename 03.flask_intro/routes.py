from flask import Flask

app = Flask(__name__)


@app.route('/') # root
def index():
    return "Welcome to our First Web Application!"


@app.route('/about')
def about():
    return "We are a couple of developers from IT Step."


@app.route('/contact')
def contact():
    return "You can reach us at about@irstep.org"


@app.route('/offerings/python')
def python():
    return "We can offer python web development as part of our portfolio"


@app.route('/offerings/cpp')
def cpp():
    return "We offer C++ drivers development as part of our portfolio"


if __name__ == '__main__':
    app.run(debug=True)
