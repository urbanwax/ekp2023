from pydantic import BaseModel, Field
from typing import Optional
from fastapi import FastAPI

api = FastAPI()


class PasswordConfig(BaseModel):
    complexity: str = Field(examples=["secure"], description="Complexity of the Passwords to be generated!")
    length: int = Field(examples=[10], description="Length of the Passwords to be generated!")


class RequestPassword(BaseModel):
    number: int = Field(examples=[12], description="The number of passwords to be generated upon the request!")
    length: Optional[int] = Field(default=5, examples=[10], description="[Optional] The length of the passwords to be generated upon teh request")


current_password_config = PasswordConfig(complexity="simple", length=10)


@api.get("/")
def rest_info():
    return {"info": "This is a sample REST API base on FastAPI framework."}


@api.get("/admin/config")
def display_config():
    return current_password_config


@api.post("/admin.config")
def set_config(req_config: PasswordConfig):
    global current_password_config
    current_password_config = req_config
    return current_password_config


@api.post("/user/passgen")
def generate_password(request: RequestPassword):
    # Implement Password Generation Logic
    return {"info": request}
