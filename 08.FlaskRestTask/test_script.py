import requests

rest_api_url = 'http://127.0.0.1:5000'
admin_addon = '/admin/update_config'
user_addon = '/generate'


def test_password_generation(pass_num, pass_len):
    url = rest_api_url + user_addon
    data = {"password_num": pass_num, "password_length": pass_len}
    response = requests.post(url, json=data)
    print(response.json())


def update_config(complexity_types):
    url = rest_api_url + admin_addon
    data = complexity_types
    response = requests.post(url, json=data)
    print(response.json())


if __name__ == '__main__':
    test_password_generation(1,1)
    test_password_generation(15,12)
    test_password_generation('asdf', 'asdf')
    update_config({
                "lowercase": False,
                "uppercase": False,
                "digits": False
            })
    test_password_generation(1, 1)