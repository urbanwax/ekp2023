from flask import Flask, render_template, request, redirect, url_for
from inventory import Dealership

app = Flask(__name__)
inv = Dealership()
inv.add_car('BMW', '330i', 2020, 'red', 'https://imgd.aeplcdn.com/1056x594/n/wpfgpta_1503247.jpg?q=80&wm=1')
inv.add_car('BMW', 'X5', 2019, 'black',
               'https://65e81151f52e248c552b-fe74cd567ea2f1228f846834bd67571e.ssl.cf1.rackcdn.com/ldm-images/2019-BMW-X5-Black-Sapphite-Metallic.png')
inv.add_car('Porsche', '911', 2022, 'grey',
               'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRzAWeXTzxc-HxHx0LAshNObWIdEyFnXC9iHg&usqp=CAU')

@app.route('/')
def home():
    cars = inv.list_available_cars()
    return render_template('home.html', cars=cars)


@app.route('/sold_cars')
def sold_cars():
    sold_cars_list = inv.list_sold_cars()
    return render_template('sold_cars.html', sold_cars_list=sold_cars_list)


@app.route('/sell_car', methods=['GET', 'POST'])
def sell_car():
    if request.method == 'POST':
        name = request.form.get('nameInput')
        age = request.form.get('ageInput')
        inv.sell_car(name, age)
        return redirect(url_for('sold_cars'))
    car_for_sale = inv.get_the_car_for_sale()
    return render_template('sell_car.html', car=car_for_sale)


@app.route('/add_car', methods=['GET', 'POST'])
def add_car():
    if request.method == 'POST':
        make = request.form.get('makeInput')
        model = request.form.get('modelInput')
        year = request.form.get('yearInput')
        color = request.form.get('colorInput')
        pic = request.form.get('picInput')
        inv.add_car(make, model, year, color, pic)
        return redirect(url_for('home'))
    return render_template('add_car.html')


if __name__ == '__main__':
    app.run(debug=True)
