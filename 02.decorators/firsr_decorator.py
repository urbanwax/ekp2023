# This is the definition of the decorator
def add_signature(func):
    def wrapper(name, bill):
        func(name, bill)
        print("Kind Regards,\nYour Mobile Operator")
        return
    return wrapper


# Decorating greet() function with the add_signature decorator
@add_signature
def greet(name: str, bill: float) -> None:
    print(f"Hello {name},\n\tYour monthly bill is {bill}")


# Decorating inform() function with add_signature decorator
@add_signature
def inform(name, bill):
    print(f"Dear {name},\n\tYou have a delayed payment for the previous month for the amount of: {bill}")


if __name__ == '__main__':
    greet('Ivo', 234.345)
    print(3*'\n')
    inform("Peter", 123)