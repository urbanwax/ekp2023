from flask import Flask, render_template
from data import menu_items, content, services, get_menu_items

app = Flask(__name__)


@app.route('/')
def home():
    return render_template('home.html', menu_items=get_menu_items(), item_content=content.get('Home'))


@app.route('/about')
def about():
    return render_template('about.html', menu_items=get_menu_items(), item_content=content.get('About'))


@app.route('/contact')
def contact():
    return render_template('contact.html', menu_items=get_menu_items(), item_content=content.get('Contact'))


@app.route('/services/<service>')
def display_service(service):
    return render_template('services.html', menu_items=get_menu_items(), servcontent=services.get(service))


if __name__ == '__main__':
    app.run(debug=True)
