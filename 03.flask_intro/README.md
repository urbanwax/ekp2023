# Introduction in Flask

Flask is a lightweight web application framework for Python.
https://flask.palletsprojects.com/en/3.0.x/

Flask in pypi.org
https://pypi.org/project/Flask/

## Installation
With the following command we install Flask and its prerequisites 
```shell
pip install flask
```

## Hello World

### Run Flask app from command line

Sample code for Hello World example.
```python
from flask import Flask

app = Flask(__name__)

@app.route('/')
def hello():
    return 'Hello World'
```
Command for running the flask app ```flask --app hello_world run``` where hello_world is the name of the main python module holding your web application.

### Run Flask app from the main python module of the web application

You just run the python file and this everything needed

```python
from flask import Flask

app = Flask(__name__)

@app.route('/')
def hello():
    return 'Hello World'

if __name__ == '__main__':
    app.run()

```

#### Arguments of **app.run()** function
- `debug=True` - automatically restarts the web server whenever it detects a change
- `port=8000` - sets the Listening port of the web server to 8000. Default is 5000.

## Flask Routes

Flask uses routes to maps URLs to functions.

The following code will map 127.0.0.1:5000/about to what returns the function about(). /Assuming  running on standard localhost and port/
```python
@app.route('/about')
def about():
    return "We are a couple of developers from IT Step."
```

## Path Variables
Path variables reside in URL and are defined in between '<' and '>' in the path argument to the route decorator.
The same should be argument of the decorated function.

Typing is also possible in path parameters using the type and colon as follows:
```python
@app.route("/user/<id:int>")
def func(id):
    return f"User id is {id}"
```
The example forces id to be integer.

## Templating
The default templating engine in Flask is Jinja2. Flask depends on Jinja2.
Official documentation: https://flask.palletsprojects.com/en/3.0.x/quickstart/#rendering-templates

For rendering HTML templates with Jinja2 we need the following:
```python
from flask import render_tremplate
```

For passing variable to the Jinja2 template we need the following:
```python
# In this example we pass the content of menu_items to the variable "menu_items" in the Jinja2 template
@app.route('/')
def home():
    return render_template('home.html', menu_items=menu_items)
```

For extending templates you can use the ***extends*** Jinja2 keyword. In the example index.html extends base.html

base.html
```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>It Step</title>
</head>
<body>
    <p> {% block content %} {% endblock %} </p>
</body>
</html>
```

index.html
```html
{% extends 'base.html' %}
{% block content %} <p>Our text goes here</p> {% endblock  %}
```

For adding images to the html templates:
- With direct url
    ```html
    <img src="{{ pic_url }}"  width="300" height="300">
    ```
    ***pic_url*** is a Jinja2 variable that contains the url of the desired picture. URLs can be absolute(somewhere in the Internet) or relative to our site like '/pics/my_pic.jpg'


- Using the static directory
  By default flask uses it static directory called ***static*** to store static content like picture, css files, js files etc. The ***static*** directory resides in the root of the project. To use pictures in the static directory you can point to them:
  - directly with the name of the files in the ***static*** directory
    ```html
    <img src="{{ url_for('static', filename='my_pic.jpg') }}"  width="300" height="300">
    ```
  - with variable (e.g. ***pic_url***) that holds the full name of the file
    ```html
    <img src="{{ url_for('static', filename=pic_url) }}"  width="300" height="300">
    ```