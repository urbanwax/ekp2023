# Fast API

FastAPI is a modern, fast (high-performance), web framework for building APIs with Python 3.8+ based on standard Python type hints.

Official Web Page
https://fastapi.tiangolo.com/

Official Documentation
https://fastapi.tiangolo.com/

Fast API in pypi.org
https://pypi.org/project/fastapi/

## Installation

Single line install. This also installs ***uvicorn***(teh web server which will run the FastAPI services)

```shell
pip install "fastapi[all]"
```

Multiline. Usually when you deploy to production.
```shell
pip install fastapi

pip install uvicorn[standard]
```

## Run a FastAPI project

```shell
# If FastAPI object is defined in 'main.py'
# If FastAPI object is named 'app'
uvicorn main:app --reload
```

Default port is 8000.

If uvicorn is started with ***--reload*** option then uvicorn will restart itself if changes are detected.

## Path Parameters
Type not specified
```python
@app.get("/books/{book_id}")
def read_book(book_id):
    return {"book": book_id}
```

book_id is integer. This validated by FastAPI
```python
@app.get("/books/{book_id}")
def read_book(book_id: int):
    return {"book": book_id}
```

book_id is string. This validated by FastAPI
```python
@app.get("/books/{book_id}")
def read_book(book_id: str):
    return {"book": book_id}
```

We can also use custom types

## Query Parameters

```python
@app.get("/books/")
def read_book(book_id: str):
    return {"book": book_id}
```

## Raising errors
```python
from fastapi import FastAPI, HTTPException

app = FastAPI

books = ["Peppa Pig", "Baby Shark"]

@app.get("/books/{book_name")
def read_book(book_name: str):
    if book_name not in books:
        raise HTTPException(status_code=404, detail="Book not found")
```

## Swagger (API Documentation)

What is swagger? https://swagger.io/

FastAPI has automatically built swagger
Swagger is available under http://localhost:8000/docs (assuming we run the FastAPI on localhost)

Redoc is avaulable under http://localhost:8000/redoc (assuming we run FastAPi on localhost)

