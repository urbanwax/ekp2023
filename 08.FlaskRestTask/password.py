import string
from random import choice


class PasswordGenerator:

    default_number_of_passwords = 5
    default_password_length = 10

    def __init__(self):
        self.config = {
            'complexity_types': {
                'lowercase': True,
                'uppercase': True,
                'digits': True
            },
            'pass_len': {'min': 5, 'max': 20},
            'pass_num': {'min': 1, 'max': 10}
        }

    def update_config(self, complexity_types):
        # Update config is only for updating whether certain type of character are included
        if not isinstance(complexity_types, dict):
            return False
        flag_config_set = False
        if isinstance(complexity_types.get('lowercase'), bool):
            self.config["complexity_types"]["lowercase"] = complexity_types.get('lowercase')
            flag_config_set = True
        if isinstance(complexity_types.get('uppercase'), bool):
            self.config["complexity_types"]["uppercase"] = complexity_types.get('uppercase')
            flag_config_set = True
        if isinstance(complexity_types.get('digits'), bool):
            self.config["complexity_types"]["digits"] = complexity_types.get('digits')
            flag_config_set = True
        return flag_config_set

    def generate_password(self, password_num, password_length):
        if not isinstance(password_num, int) and \
                password_num not in range(self.config.get('pass_num').get('min'), self.config.get('pass_num').get('max')):
            password_num = PasswordGenerator.default_number_of_passwords
        if not isinstance(password_length, int) and \
                password_length not in range(self.config.get('pass_len').get('min'), self.config.get('pass_len').get('max')):
            password_length = PasswordGenerator.default_password_length

        def gen_pass(pass_len, lowercase, uppercase, digits):
            allowed_characters = ''
            if lowercase:
                allowed_characters += string.ascii_lowercase
            if uppercase:
                allowed_characters += string.ascii_uppercase
            if digits:
                allowed_characters += string.digits
            return ''.join([choice(allowed_characters) for _ in range(pass_len)])

        return [gen_pass(password_length, self.config.get("complexity_types").get("lowercase"),
                         self.config.get("complexity_types").get("uppercase"),
                         self.config.get("complexity_types").get("digits"))
                for _ in range(password_num)]


if __name__ == '__main__':
    test_pass_gen = PasswordGenerator()
    print(test_pass_gen.generate_password(12, 16))
    print(test_pass_gen.generate_password('22', '22'))
    print('Config Updated: ', test_pass_gen.update_config({
                'lowercase': 'ewfr',
                'uppercase': False,
                'digits': True
            }))
    print(test_pass_gen.config)
    print(test_pass_gen.generate_password(10, 20))
    print(test_pass_gen.generate_password('22', '22'))