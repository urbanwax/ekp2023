from uuid import uuid4

class Car:
    def __init__(self, make, model, year, color, img):
        self._id = str(uuid4())
        self._is_sold = False
        self._make = make
        self._model = model
        self._year = year
        self._color = color
        self._img = img

    def to_dict(self):
        car_dict =  {
            'id': self._id,
            'make': self._make,
            'model': self._model,
            'year': self._year,
            'color': self._color,
            'pic': self._img
        }
        if self.is_sold():
            car_dict['owner_name'] = self._owner_name
            car_dict['owner_age'] = self._owner_age
        return car_dict

    def sell(self, name, age):
        self._owner_name = name
        self._owner_age = age
        self._is_sold = True

    def is_sold(self):
        return self._is_sold


if __name__ == '__main__':
    car1 = Car('BMW', '330i', 2020, 'red', 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTF5HwlfT9ZGqmEoI33AhElORW5HdTvUG1SI9QReZlv2w&s')
    print(car1.to_dict())
    car2 = Car('BMW', 'X5', 2019, 'black',
               'https://65e81151f52e248c552b-fe74cd567ea2f1228f846834bd67571e.ssl.cf1.rackcdn.com/ldm-images/2019-BMW-X5-Black-Sapphite-Metallic.png')
    print(car2.to_dict())

    car1.sell('Ivo Ivanov', 39)
    print(car1.to_dict())
