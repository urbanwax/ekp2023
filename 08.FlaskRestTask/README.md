# Rest Api for Password Generation

- Rest API will use Python and Flask
- The service should have admin and user sections
- admin section is used to update password generation configuration
    - admin should be able to modify the complexity of the passwords( whether it includes different types of character at least 3 it is up to you which one)
    - all data should be validated
- user section is used to retrieve the desired number of generated passwords
    - when requesting passwords the user should request how many passwords needs to be generated (if not successful the default is 5, also user should be able to request only between 1 and 10 password)
    - when requesting passwords the user should request the desired length of passwords (should be between 5 and 20 characters)
    - all data should be validated
- passwords generation and configuration should be maintained by a single class. Default password configuration is up to you
- demonstrate with a separate python script how you retrieve passwords the change the configuration multiple times, demonstrate also proper validation


