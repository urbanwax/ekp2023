from fastapi import FastAPI
from pydantic import BaseModel, Field

app = FastAPI()

books = {}


class Book(BaseModel):
    title: str
    author: str
    year: int

    model_config = {
        "json_schema_extra": {
            "examples": [
                {
                    "title": "The Three Musketeers",
                    "author": "Alexandre Dumas",
                    "year": 1844,
                },
            ]
        }
    }


class Book1(BaseModel):
    title: str = Field(
        examples=["The Three Musketeers"],
        title="Title of the Book",
        description="Title of the Book",
        min_length=2,
        max_length=20
    )
    author: str = Field(examples=["Alexandre Dumas"])
    year: int = Field(examples=[1844], gt=360)


@app.get("/")
def info():
    return {"Info": "Sample Fast Api Rest API"}


@app.post("/books/{book_id}")
def create_book(
        item_id: int,
        book: Book1):
    books[item_id] = book
    return {"status": "success"}


@app.get("/books")
def list_books():
    return {"books": books}


@app.get("/books/{book_id}", response_model=Book1)
def get_book_by_id(book_id: int):
    book = books.get(book_id)
    if book:
        return book
