import requests
from flask import Flask, request, jsonify
from password import PasswordGenerator


api = Flask(__name__)
rest_pass_gen = PasswordGenerator()

@api.post('/generate')
def gen_pass():
    try:
        req = request.json
        password_num = req.get('password_num')
        password_length = req.get('password_length')
        gen_data = rest_pass_gen.generate_password(12, 12)
        return jsonify({'generated passwords': gen_data}), 200
    except Exception as e:
        return jsonify({'error': str(e)}), 400

@api.post('/admin/update_config')
def update_config():
    try:
        req = request.json
        update_result = rest_pass_gen.update_config(req)
        return jsonify({'config updated': update_result}), 200
    except Exception as e:
        return jsonify({'error': str(e)}), 400


if __name__ == '__main__':
    api.run(debug=True)
