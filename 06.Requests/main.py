import requests

r = requests.get("https://swapi.dev/api/people/1/")

if r.status_code == 200:
    print('Site is UP')
    person = r.json()
    print(type(person))
    for k, v in person.items():
        print(f"{k}={v}")
else:
    print('Some Error Occurred!')
