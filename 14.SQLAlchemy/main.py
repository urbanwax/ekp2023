import sqlalchemy as db
from random import choice


engine = db.create_engine("sqlite:///my_database.sqlite")

conn = engine.connect()
meta = db.MetaData()

people_table = db.Table(
    'people', meta,
    db.Column('id', db.Integer, primary_key=True),
    db.Column('first_name', db.String),
    db.Column('last_name', db.String),
    sqlite_autoincrement=True
)

meta.create_all(engine)

# Insert Record
fnames = ["Ivo", "Ivan", "Peter", "Andrey"]
lnames = ["Ivanov", "Petrov", "Dimitrov"]
for _ in range(10):
    person1 = people_table.insert().values(first_name=choice(fnames), last_name=choice(lnames))
    conn.execute(person1)
    conn.commit()

# Read (Select) Record
# Select All
records = conn.execute(people_table.select())

for rec in records:
    print(rec)

# Where clause
records = conn.execute(db.select(people_table).where(people_table.c.last_name=="Ivanov"))

for rec in records:
    print(rec)

# Where clause specific columns select
stmt = db.select(people_table.c.id).where(people_table.c.last_name=="Dimitrov")
print(stmt)
records = conn.execute(stmt)
for rec in records:
    print(rec)

# Where with SQL statement
stmt = 'SELECT p.id, p.first_name FROM people as p WHERE p.last_name == "Dimitrov"'
print(stmt)
records = conn.execute(db.text(stmt))
for rec in records:
    print(rec)