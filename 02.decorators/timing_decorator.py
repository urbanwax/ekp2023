import time
import random
from string import ascii_letters

def calculate_timing(func):
    def wrapper(*args, **kwargs):
        start = time.time()  # define start - time before function call
        result = func(*args, **kwargs)  # function call
        end = time.time()  # define end - time after function call/run
        print(f"Running function {func.__name__}() took {end-start} seconds.")
        return result
    return wrapper

@calculate_timing
def average_list(lst):
    return sum(lst) / len(lst)


@calculate_timing
def gen_text():
    return ''.join(random.choice(ascii_letters) for _ in range(10000))


if __name__ == '__main__':
    l1 = [random.randint(1, 100) for _ in range(100000)]
    print(average_list(l1))
    print(gen_text())
