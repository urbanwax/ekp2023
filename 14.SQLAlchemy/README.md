# SQLAlchemy

## What is ORM?
ORM stands for Object-relational model: map Python(not only Python but Programming Language Objects) Objects to your database

## SQLAlchemy
[SQLAlchemy](https://www.sqlalchemy.org/)

### Install SQLAlchemy
```shell
pip install sqlalchemy
```

### Select statements
https://docs.sqlalchemy.org/en/20/tutorial/data_select.html

### Insert statements
https://docs.sqlalchemy.org/en/20/tutorial/data_insert.html

### Update and Delete statements
https://docs.sqlalchemy.org/en/20/tutorial/data_update.html




