from item import Car
from queue import InvQueue


class Dealership:
    def __init__(self):
        self._available_cars = InvQueue()
        self._sold_cars = []

    def add_car(self, make, model, year, color, img):
        car = Car(make, model, year, color, img)
        self._available_cars.add(car)

    def sell_car(self, name, age):
        car = self._available_cars.get()
        car.sell(name, age)
        self._sold_cars.append(car)
        return car.to_dict()

    def list_available_cars(self):
        cars = self._available_cars.list()
        return [car.to_dict() for car in cars]

    def list_sold_cars(self):
        return [car.to_dict() for car in self._sold_cars]

    def get_the_car_for_sale(self):
        if not self._available_cars.is_empty():
            return self._available_cars.display_last().to_dict()
        return None

if __name__ == '__main__':
    dlrs = Dealership()
    print(dlrs.list_available_cars()) # should return an empty list
    print(dlrs.get_the_car_for_sale())
    dlrs.add_car('BMW', '330i', 2020, 'red', 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTF5HwlfT9ZGqmEoI33AhElORW5HdTvUG1SI9QReZlv2w&s')
    print(dlrs.get_the_car_for_sale())
    dlrs.add_car('BMW', 'X5', 2019, 'black',
               'https://65e81151f52e248c552b-fe74cd567ea2f1228f846834bd67571e.ssl.cf1.rackcdn.com/ldm-images/2019-BMW-X5-Black-Sapphite-Metallic.png')
    print(dlrs.list_available_cars())
    print(dlrs.get_the_car_for_sale())
    print(dlrs.sell_car('Ivo', 39))
    print(dlrs.list_available_cars())
    print(dlrs.get_the_car_for_sale())
    print(dlrs.sell_car('Miro', 23))
    print(dlrs.list_available_cars())
    print(dlrs.get_the_car_for_sale())
    print(dlrs.list_sold_cars())