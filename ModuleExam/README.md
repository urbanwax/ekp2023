# Exam Task

The task will contain two main services. Web App and Rest API

## Web App

- Frameworks: FastAPi or Flask
- Languages: Python, Jinja2, HTML, CSS
- Database: False
- Description: The web app will consume the REST API and will handle:
  - creation and deletion of:
    - users
    - objects
    - policies
  - list in a html table
    - all users
    - all objects
    - all policies for several users( users input in html way )
    - (Optional) all policies per object selected from a Dropdown
- Logging: False
- Docstrings: True
- Swagger: False
- Examples in Swagger: False

* Remarks: use requests to consume the REST API. Try to implement some kind of web design (e.g. use Bootstrap). Implement Menu.

## Rest API

- Frameworks: FastAPi
- Languages: Python
- Database: True (SQLite or MySQL or Postgres)
- Description: The service will handle:
  - creation and deletion of:
    - users(id, First Name, Last Name, username, email). Cannot delete user with existing policies. Username and email creation to be base on the Names of the user and will be unique.
    - objects(id, path). Can delete object wit existing policies but will also delete all policies related to the objects.
    - policies: mapping of users to objects/who will be able to access which objects/. No restrictions on deletion of policies.
  - list of(think of GET method):
    - users
    - objects
    - policies per user
    - policies per object
- Logging: True
- Docstrings: True
- Swagger: True
- Examples in Swagger: True

## Examples
Users:
- Ivo Ivanov iivanov iivanov@gmail.com
- Peter Ivanov piv piv@gmail.com
- Ivan Ivanov iiv iiv@gmail.com
- Ivan Panov paniv paniv@gmail.com

Objects:
- /path/dir1/obj1
- /path/dir1/obj2
- /path/directory/obj221

Policies:
piv -> /path/dir1/obj1
piv -> /path/dir1/obj2
iiv -> /path/directory/obj221
iivanov -> /path/dir1/obj1
iivanov -> /path/dir1/obj2
iivanov -> /path/directory/obj221

## Data Model
```mermaid
erDiagram
    USER {
        int id PK
        string username
        string firstName
        string lastName
        string email

    }
    OBJECT {
        int id PK
        string path
    }
    POLICY {
        int id PK
        int policy_id
        string name
        int object_id FK "points to id in OBJECT"
    }
    USER_POLICY {
        int id PK
        int user_id FK "points to id in USER"
        int policy_id FK "points to policy_id in POLICY"
    }
    USER ||--o{ USER_POLICY : is
    POLICY ||--o{ USER_POLICY : is
    OBJECT ||--o{ POLICY : is
```