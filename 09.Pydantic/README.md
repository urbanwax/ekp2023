# Pydantic

## General
Pydantic is a data validation library for Python based on type annotations.
Pydantic official website - https://docs.pydantic.dev/latest/.
Pydantic is not built-in library for Python, so it needs to be installed.
```shell
pip install pydantic
```


Built-in Python package with similar functionality is Dataclasses(https://docs.python.org/3/library/dataclasses.html)

## BaseModel

This is the class that all our data models should inherit
```python
from pydantic import BaseModel

class Student(BaseModel):
    id: int
    name: str
    grade: float

sample_data = {
        'id': 3,
        'name': False,
        'grade': 5.7
    }

student_pydantic = Student(**sample_data)
print(student_pydantic.dict())
```

## Lists, Tuples etc.

import necessary data structures from `typing` module.
Example in `datamodel_with_structure.py`.

## Nested Data models

Data model can be nested. One data model defined can be a field in another data model.