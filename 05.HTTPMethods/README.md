# HTTP Methods and Status Codes

HTTP(Hypertext Transfer Protocol) is the foundation of data communication on the World Wide Web(WWW). This is the protocol that defines how messages are formatted  and transmitted and how web servers and clients(browsers and others) should respond to different commands.
In HTTP, there are several methods (also known as verbs) that describe the action to be performed on the given resource. These methods are used to indicate the desired operation when making requests to a given web server.

## Common HTTP Methods

1. GET
    - The **GET** method is used to request data from a specified resource.
    - It is typically used got fetching information or data from web server.
    - Common Status Codes:
      - 200 (OK) - The request is successful.
      - 404 (Not Found) - The requested resource was not found.
2. POST
   - The **POST** method is used to submit data to be processed to a specified resource.
   - It is often used when submitting form data or when uploading a file.
   - Common Status Codes:
     - 201 (Created) - The resource has been created.
     - 400 (Bad Request) - The request is malformed or invalid.
3. PUT
   - The **PUT** method is used to update a current resource with new data.
   - It is idempotent, meaning that multiple identical request will have the same effect as a single request.
   - Common Status Codes:
     - 200 (OK) - The request is successful.
     - 204 (No Content) - The request is successful, but there is no response body.
4. DELETE
   - The **DELETE** method is used to request a removal of a resource.
   - It is often used to delete data or resource on the web server.
   - Common Status Code:
     - 200 (OK) - The request was successful.
     - 404 (Not Found) - The resource to be deleted was not found.
5. PATCH
   - The **PATCH** method is used to apply partial modifications to a resource.
   - It is often used to update specific fields of an existing resource.
   - Common Status Codes:
     - 200 (OK) - The request was successful.
     - 422 (Unprocessable Entity) - The request is semantically incorrect
6. HEAD
   - The **HEAD** method is very similar to the **GET** method but only retrieves the headers of the response without the actual data.
   - It is often used to check what methods and other capabilities the server supports.
     - 200 (OK) -  The request was successful.
     - 404 (Not Found) - The requested resource was not found.
7. OPTIONS
   - The **OPTIONS** method is used to describe the communication options for the target resource.
   - It is often used to check what methods and other capabilities teh server supports. 
     - 200 (OK) -  The request was successful.
     - 204 (No Content) - The request is successful, but there is no response body.
8. CONNECT
   - The **CONNECT** method established a network connection to a resource identified by the URI.
   - It is typically used for creating network tunnels to secure resources through an HTTP Proxy.
9. TRACE
   - The **TRACE** method performs a diagnostic test along the path to the target resource.
   - It is used for debugging and troubleshooting purposes. 

## HTTP Status Codes
Http status codes are returned by the server to indicate the result of a submitted request.

- 1xx (Informational)
- 2xx (Successful)
- 3xx (Redirected)
- 4xx (Client Error)
- 5xx (Server Error)