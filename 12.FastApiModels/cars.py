from pydantic import BaseModel, Field
from fastapi import FastAPI, Body
from typing import Annotated, List

api = FastAPI()

cars = []


class CarIn(BaseModel):
    reg_plate: str = Field(min_length=7, max_length=8, examples=["A2345KK"])
    make: str = Field(min_length=2, examples=["BMW"])
    model: str = Field(min_length=2, examples=["318i"])


class CarOut(BaseModel):
    id: int = Field(ge=0, examples=[1])
    reg_plate: str = Field(min_length=7, max_length=8, examples=["A2345KK"])
    make: str = Field(min_length=2, examples=["BMW"])
    model: str = Field(min_length=2, examples=["318i"])


@api.get("/car/{car_id}", response_model=CarOut)
def display_car(car_id: int):
    obtained = cars[car_id]
    car = CarOut(id=car_id, **obtained.dict())
    return car


@api.post("/car", response_model=CarOut)
def create_car(car: Annotated[CarIn, Body()]):
    cars.append(car)
    return_car = CarOut(id=len(cars)-1, **car.dict())
    return return_car


@api.get("/cars", response_model=List[CarIn])
def list_cars():
    return cars
