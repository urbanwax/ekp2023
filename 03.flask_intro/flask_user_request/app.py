from flask import Flask, render_template, request, url_for, redirect

app = Flask(__name__)

@app.route('/', methods=["GET", "POST"])
def index():
    if request.method == 'POST':
        name = request.form['name']
        return redirect(url_for('result', input_name=name))
    return render_template('index.html')

@app.route('/result')
def result():
    return render_template('result.html', user_input=request.args.get('input_name'))

@app.get('/v2')
def index2():
    return render_template('index2.html', username=request.args.get('input_name'))

@app.post('/v2')
def postindex2():
    name = request.form['name']
    return redirect(url_for('index2', input_name=name))


if __name__ == '__main__':
    app.run(debug=True)
