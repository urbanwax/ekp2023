from flask import Flask, jsonify, request

api = Flask(__name__)

capabilities = ['Python', 'Java', 'C#', 'C', 'C++']


@api.get('/',)
def get_api_info():
    """
    Generates basic information related to the REST API Service
    """
    api_info = {
        'version': '1.0',
        'Main Programing Language': 'Python',
        'Framework': 'Flask',
        'Author': 'IT Step Computer Academy',
        'Autor email': 'info@itstep.org',
    }
    return jsonify(api_info)


@api.get('/capabilities')
def list_capabilities_all():
    """
    List all capabilities with their respective number
    """
    capabilities_dict = {cap[0]: cap[1] for cap in enumerate(capabilities)}
    return jsonify(capabilities_dict)


@api.get('/capability/<int:id>')
def list_capability_single(id):
    """
    List a single capability by its ID
    """
    return jsonify(capabilities[id])


@api.post('/capabilities')
def add_capabilities_many():
    """
    Will add multiple capabilities.
    Remark: will not add duplicate capabilities
    """
    new_capabilities = request.json
    capabilities_added = []
    if isinstance(new_capabilities, list):
        for cap in new_capabilities:
            if isinstance(cap, str) and cap not in capabilities:
                capabilities.append(cap)
                capabilities_added.append(cap)
    return jsonify({"Added Capabilities": capabilities_added})


@api.post('/capability')
def add_capability_single():
    """
    Will add a single capability
    Remark: will not add duplicate capability
    """
    new_capability = request.json
    response = {"Added Capability": "None"}
    if isinstance(new_capability, str) and new_capability not in capabilities:
        capabilities.append(new_capability)
        response = {"Added Capability": new_capability}
    return jsonify(response)


@api.delete('/capability/<int:id>')
def delete_capability_single_id(id):
    """
    Will delete a single capability appointed by its ID
    Remark: Will indicate if deletion is not successful
    """
    try:
        deleted_capability = capabilities.pop(id)
        response = {"Deleted Capability": deleted_capability}
    except Exception as e:
        response = {"Deleted Capability": "None"}
    return jsonify(response)


@api.delete('/capability/<name>')
def delete_capability_single_name(name):
    """
    Will delete a single capability appointed by its Name
    Remark: Will indicate if deletion is not successful
    """
    try:
        deleted_capability = capabilities.remove(name)
        response = {"Deleted Capability": name}
    except Exception as e:
        response = {"Deleted Capability": "None"}
    return jsonify(response)


if __name__ == '__main__':
    api.run(debug=True)
