from jinja2 import Environment, FileSystemLoader
from config import services, custom, webpage, templates_dir, output_index, output_custom, source_custom, source_index

if __name__ == '__main__':
    # We load all the source templates from the folder
    environment = Environment(loader=FileSystemLoader(templates_dir))
    template = environment.get_template(source_index)
    html_source = template.render(webpage=webpage, services=services)
    # Generated webpage will reside in the output folder
    with open(output_index, mode="w", encoding="utf-8") as webpage:
        webpage.write(html_source)

    # Here we will start generating out second html page
    template = environment.get_template(source_custom)
    html_source = template.render(webpage=webpage, custom=custom)
    with open(output_custom, mode="w", encoding="utf-8") as custom_page:
        custom_page.write(html_source)

