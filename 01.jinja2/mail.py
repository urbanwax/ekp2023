from jinja2 import Environment, FileSystemLoader

if __name__ == '__main__':
    # Initialize variable to be used within the Jinja2 Template
    people = ['Ivo Ivanov', 'Maria Kirilova', 'Iliya Penev', 'Kostadin Dimitrov']
    product = 'PlayStation 5'

    # Configure Jinja2 Engine
    templates_path = "templates/"
    environment = Environment(loader=FileSystemLoader(templates_path))
    template = environment.get_template("mail.txt")
    for person in people:
        output = template.render(name=person, product=product)
        print(output)
        print(50 * '-')
