# Rest Api for Password Generation

- Rest API will use Python and FastAPI
- The consumer(user) will be able to retrieve a password or number of passwords
  - number of passwords is requested by the consumer (2 endpoindts one will only for one password and the other for more)
  - password length is requested by the consumer
  - complexity of password(which of the 3 groups of characters it will contain, small and capital letters and digits)
