from pydantic import BaseModel, ValidationError


class StudentStd:
    def __init__(self, id, name, grade):
        if isinstance(id, int):
            self.id = id
        else:
            raise ValueError("ID should be int")
        if isinstance(name, str):
            self.name = name
        else:
            raise ValueError("Name should be str")
        if isinstance(grade, float):
            self.grade = grade
        else:
            raise ValueError("Grade should be float")


class Student(BaseModel):
    id: int
    name: str
    grade: float


if __name__ == '__main__':
    sample_data = {
        'id': 3,
        'name': False,
        'grade': 5.7
    }

    try:
        student_standard = StudentStd(**sample_data)
        print(student_standard.__dict__)
    except ValueError as v:
        print(v)

    try:
        student_pydantic = Student(**sample_data)
        print(student_pydantic.dict())
    except ValueError as e:
        print(e)
        print(10*'+')
        errors = e.errors()
        print(errors[0])
        print("loc", errors[0]["loc"])
        print("msg", errors[0]["msg"])
