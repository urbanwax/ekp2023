from jinja2 import Environment, FileSystemLoader

if __name__ == '__main__':
    # Initialize variable to be used within the Jinja2 Template
    team = ['Ivo', 'Dimitar', 'Maria']

    # Configure Jinja2 Engine
    templates_path = "templates/"
    environment = Environment(loader=FileSystemLoader(templates_path))
    template = environment.get_template("team.txt")
    # passing the python list team to the names variable in the Jinja2 template
    output = template.render(names=team)
    print(output)
