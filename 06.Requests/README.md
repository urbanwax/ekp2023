# Requests

Python module for handling HTTP requests

# Documentation
Official documentation
https://requests.readthedocs.io/en/latest/

# Installation
```shell
pip install requests
```