menu_items = {
    'Home': '/',
    'About': '/about',
    'Contact': '/contact'
}

content = {
    'Home': {'title': 'Welcome to HomePage', 'text': 'IT Step Computer academy web page. Welcome to IT Step web page. Here you will find various information for the services that we offer.', 'pic': 'team.jpg'},
    'About': {'title': 'About Us', 'text': 'IT Step Computer academy web page. We are an young IT company situated in Bulgaria.', 'text1': 'Something additional for us is that we are vere keen on keeping our talented engineers.'},
    'Contact': {'title': 'Contact Us', 'text': 'You can reach us at info@itstep.org'},
}

services = {
    'Web': {'title': 'Web Development', 'text': 'We can provide web developing services for anything from simple Web pages to complex Web Apps.', 'logo': 'https://img.freepik.com/premium-vector/blue-square-with-globe-icon-it_876006-15.jpg'},
    'Android': {'title': 'Android Development', 'text': 'We can develop your apps for the Android Platform.', 'logo': 'https://upload.wikimedia.org/wikipedia/commons/thumb/3/3e/Android_logo_2019.png/599px-Android_logo_2019.png?20190822201450'},
    'IOS': {'title': 'IOS Development', 'text': 'We can develop your apps for the IOS Platform.', 'logo': 'https://logowik.com/content/uploads/images/ios1780.logowik.com.webp'},
}

def get_menu_items():
    new_menu_items = dict()
    new_menu_items['Home'] = menu_items.get('Home')
    for item in services.keys():
        new_menu_items[item] = f"/services/{item}"
    new_menu_items['About'] = menu_items.get('About')
    new_menu_items['Contact'] = menu_items.get('Contact')
    return new_menu_items
