from jinja2 import Environment

# This string will serve as a template for the Jinja2 engine
template_str = """Personal info:
name: {{ person.name }}
age: {{ person.age }}
height: {{ person.height }} """


# The Dictionary that we will display its info in Jinja2 template output
person = {
    "name": "Ivo",
    "age": 39,
    "height": 175
}

environment = Environment()
template = environment.from_string(template_str)
print(template.render(person=person))
